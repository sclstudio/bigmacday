<?php

return [
    'action'        => 'Aksi',
    'manage'        => 'Sunting',
    'add'           => 'Tambah',
    'create'        => 'Buat Baru',
    'edit'          => 'Ubah',
    'delete'        => 'Hapus',
    'search'        => 'Cari',
    'save'          => 'Simpan',
    'save_continue' => 'Simpan &amp; Lanjutkan',
    'cancel'        => 'Batal',
    'login'         => 'Login',
    'logout'        => 'Logout',
    'register'      => 'Daftar',
];
