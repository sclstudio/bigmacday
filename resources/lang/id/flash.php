<?php

return [
    'created'                => 'Data berhasil ditambahkan',
    'updated'                => 'Data berhasil diperbarui',
    'deleted'                => 'Data berhasil dihapus',
    'form_validation_failed' => 'Silakan cek kembali isian Anda',
];
