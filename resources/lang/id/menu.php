<?php

return [
    'admin' => [
        'home'     => 'Home',
        'reports'  => 'Laporan',
        'users'    => 'Pengguna',
        'settings' => 'Pengaturan',
        'logout'   => 'Logout',
    ],
    'my'    => [
        'dropdown' => [
            'profile'        => 'Profil',
            'settings'       => 'Pengaturan',
            'administration' => 'Admin Panel'
        ],
        'settings' => [
            'profile' => 'Profil',
            'account' => 'Akun',
            'email'   => 'Email',
            'log'     => 'Log',
        ]
    ]
];
