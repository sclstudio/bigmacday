<?php

return [
    'remember'           => 'Ingat saya di komputer ini',
    'activation_needed'  => 'Akun berhasil dibuat. Silakan cek email Anda untuk melakukan aktivasi.',
    'activation_subject' => 'Aktivasi Akun',
    'click_to_activate'  => 'Klik link berikut untuk aktivasi akun: :link.',
    'activation_success' => 'Akun berhasil diaktivasi, Anda sekarang bisa login.',
];
