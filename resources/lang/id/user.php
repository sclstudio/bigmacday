<?php

return [
    'name'             => 'Nama',
    'email'            => 'Email',
    'password'         => 'Sandi',
    'status'           => 'Status',
    'roles'            => 'Role',
    'registered'       => 'Terdaftar',
    'registered_since' => 'Terdaftar pada :date',
    'registration'     => [
        'send_email' => 'Kirimkan pemberitahuan ke alamat email',
        'success'    => 'Pengguna baru <strong>:name</strong> berhasil ditambahkan'
    ],
    'about'            => 'Bio',
    'website'          => 'Website',
];
