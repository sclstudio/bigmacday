@extends('my.layouts.main')

@section('content')
    {!! Form::open([
        'method' => 'put',
        'route' => ['my.profile'],
        'class' => 'ui form'
    ]) !!}
    {{ csrf_field() }}

    <div class="field">
        <label>@lang('user.name')</label>
        {!! Form::text('name', old('name', $user['name'])) !!}
    </div>
    <div class="field">
        <label>@lang('user.about')</label>
        {!! Form::text('about', old('about', $user['about'])) !!}
    </div>
    <div class="field">
        <label>@lang('user.website')</label>
        {!! Form::text('website', old('website', $user['website'])) !!}
    </div>
    <div class="ui divider hidden"></div>
    <button class="ui button" type="submit" name="submit" value="1">@lang('action.save')</button>
    {!! Form::close() !!}
@endsection
