@extends('layouts.frontend.plain')

@section('content')

    <div class="ui segment very padded">
        <a href="{{ url('/oauth/facebook') }}" class="ui button facebook"><i class="facebook icon"></i> Login Dengan Facebook</a>

        <div class="ui divider horizontal section">Atau</div>

        <form class="ui form" method="POST" action="{{ url('/auth/login') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="ui field left icon input big">
                <input type="email" name="email" placeholder="@lang('user.email')" value="{{ old('email') }}">
                <i class="mail icon"></i>
            </div>
            <div class="ui field left icon input big">
                <input type="password" name="password" placeholder="@lang('user.password')">
                <i class="lock icon"></i>
            </div>
            <div class="ui equal width grid field">
                <div class="column left aligned">
                    <div class="ui checkbox big">
                        <input type="checkbox" name="remember">
                        <label>@lang('auth.remember')</label>
                    </div>
                </div>
                <div class="column right aligned">
                    <a href="{{ url('/password/email') }}">Lupa password</a>
                </div>
            </div>
            <div class="ui field">
                <button type="submit" class="ui big fluid button">@lang('action.login')</button>
            </div>
        </form>

    </div>

    <div class="ui list small">
    </div>
    <div class="item">Belum punya akun? <a href="{{ url('auth/register') }}">Daftar disini</a></div>
    </div>

@endsection
