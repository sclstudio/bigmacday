@extends('layouts.base')

@section('body')
    @include('frontend.elements.header')
    @include('elements.flash')

    <div id="main-content">
        @yield('content')
    </div>

    @include('frontend.elements.footer')
@endsection
