<?php
namespace App\Repositories;

use App\Enum\UserStatus;
use App\User;

class UserRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return User::class;
    }

    /**
     * @param array $data
     * @param array $roles
     * @return mixed
     */
    public function createWithRoles(array $data, array $roles)
    {
        $user =  parent::create($data);
        collect($roles)->each(function($role) use ($user){
            $user->attachRole($role);
        });

        return $user;
    }

    /**
     * @param $id
     * @param array $data
     * @param array $roles
     * @return mixed
     */
    public function updateWithRoles($id, array $data, array $roles)
    {
        $user = $this->normalizeUser($id);

        $updated = parent::update($data, $user->getKey());

        if($updated) {
            $user->detachAllRoles();
            collect($roles)->each(function($role) use ($user){
                $user->attachRole($role);
            });
        }

        return $user;
    }

    public function activate($id)
    {
        $user = $this->normalizeUser($id);

        $user->status = UserStatus::ACTIVE();
        return $user->save();
    }

    protected function normalizeUser($id)
    {
        $user = $id;

        if(! $user instanceof User) {
            $user = $this->model->findOrFail($id);
        }

        return $user;
    }

}
