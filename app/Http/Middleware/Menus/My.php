<?php

namespace App\Http\Middleware\Menus;

use Caffeinated\Menus\Facades\Menu;
use Closure;

class My
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('my.settings', function($menu) {
            $menu->add(trans('menu.my.settings.profile'), route('my.profile'))->active('my/profile/*');
            $menu->add(trans('menu.my.settings.account'), route('my.account'))->active('my/account/*');
            $menu->add(trans('menu.my.settings.email'), route('my.email'))->active('my/email/*');
            $menu->add(trans('menu.my.settings.log'), route('my.log'))->active('my/log/*');
        });

        return $next($request);
    }
}
