<?php

namespace App\Http\Middleware\Menus;

use Caffeinated\Menus\Builder;
use Caffeinated\Menus\Facades\Menu;
use Closure;

class MyDropdown
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('my.dropdown', function(Builder $menu) {
            $user = auth()->user();
            $menu->add(trans('menu.my.dropdown.profile'), $user['permalink']);
            $menu->divide();
            $menu->add(trans('action.logout'), url('auth/logout'));
        });

        return $next($request);
    }
}
