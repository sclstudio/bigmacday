<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->email,
        'password'       => 'laravolt',
        'remember_token' => str_random(10),
        'status'         => \App\Enum\UserStatus::PENDING()
    ];
});

$factory->defineAs(App\User::class, 'active', function ($faker) use ($factory) {
    $user = $factory->raw(App\User::class);
    return array_merge($user, ['status' => \App\Enum\UserStatus::ACTIVE()]);
});

$factory->defineAs(App\User::class, 'blocked', function ($faker) use ($factory) {
    $user = $factory->raw(App\User::class);
    return array_merge($user, ['status' => \App\Enum\UserStatus::BLOCKED()]);
});
