<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(\App\User::class)->create(['email' => 'user.pending@laravolt.com'])->attachRole(\Bican\Roles\Models\Role::whereSlug('user')->first());
        factory(\App\User::class, 'active')->create(['email' => 'user.active@laravolt.com'])->attachRole(\Bican\Roles\Models\Role::whereSlug('user')->first());
        factory(\App\User::class, 'blocked')->create(['email' => 'user.blocked@laravolt.com'])->attachRole(\Bican\Roles\Models\Role::whereSlug('user')->first());


        factory(\App\User::class, 50)->create()->each(function ($user) {
            $user->attachRole(\Bican\Roles\Models\Role::whereSlug('user')->first());
        });

        factory(\App\User::class, 'active', 50)->create()->each(function ($user) {
            $user->attachRole(\Bican\Roles\Models\Role::whereSlug('user')->first());
        });

        factory(\App\User::class, 'blocked', 50)->create()->each(function ($user) {
            $user->attachRole(\Bican\Roles\Models\Role::whereSlug('user')->first());
        });

        Model::reguard();
    }
}
